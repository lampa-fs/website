/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./node_modules/flowbite/**/*.js",
    "../../layouts/**/*.html",
    "../../content/**/*.md"
  ],
  theme: {
    extend: {
      colors: {
        lampa_green: '#39FF14',
        lampa_cyan: '#00FFFF',
        lampa_pink: '#FF69B4',
        lampa_black: '#000000',
        lampa_gray : '#E5E4E2',
      },
      fontFamily: {
        sans: ['Open Sans', 'sans-serif'],
        serif: ['PT Serif', 'serif'],
      },
    },
  },
  plugins: [
    require('flowbite/plugin')
  ],
}

