# LAMPA website
link: [https://lampa.dvorsak.xyz/](https://lampa.dvorsak.xyz/)

## Runner
To update runner image:
```console
podman build -t registry.gitlab.com/lampa-fs/website .
```
To upload updated image:
```console
podman push registry.gitlab.com/lampa-fs/website
```


## Development
### Tailwind CSS
```console
cd assets/flowbite
npx tailwindcss -i ./src/input.css -o ../../static/output.css --watch
```

### Hugo
```console
hugo serve
```