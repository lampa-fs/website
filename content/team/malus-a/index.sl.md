---

slug: malus-a
title: asist.
name: Andreja Malus
role: assistant
photo: photo.png
fields:
- Večagentni sistemi
- Spodbujevalno učenje
- in njihova uporaba v robotiki in proizvodnih sistemih
career:
-
  title: LAMPA, članica
  year: 2021
  institution: Univerza v Ljubljani
-
  title: LAKOS, članica
  year: 2016
  institution: Univerza v Ljubljani

---

