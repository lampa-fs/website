---

slug: zuzek-t
title: asist. dr.
name: Tena Žužek
role: assistant
photo: photo.jpg
fields:
- Krmilni sistemi
- Umetna inteligenca
- Strojni vid
career:
-
  title: LAMPA, članica
  year: 2021
  institution: Univerza v Ljubljani
-
  title: LAPS, članica
  year: 2017
  institution: 
-
  title: magistrica inženirka strojništva
  year: 2017
  institution: Univerza v Ljubljani
-
  title: diplomirana fizičarka (UN)
  year: 2015
  institution: Univerza v Ljubljani


---

