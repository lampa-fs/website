---

slug: vrabic-r
title: doc. dr.
name: Rok Vrabič
role: lecturer
photo: photo.jpg
fields:
- Krmilni sistemi
- Umetna inteligenca
- Strojni vid
career:
-
  title: LAMPA, vodja laboratorija
  year: 2021
  institution: Univerza v Ljubljani
-
  title: redni profesor
  year: 2021
  institution: Univerza v Ljubljani
-
  title: izredni profesor
  year: 2015
  institution: Univerza v Ljubljani
-
  title: LPA, vodja laboratorija
  year: 2009
  institution: 
-
  title: docent
  year: 2008
  institution: Univerza v Ljubljani
-
  title: doktorat
  year: 2004
  institution: Univerza v Ljubljani
-
  title: 
  year: 
  institution: 


---

