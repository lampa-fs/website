---

slug: podrzaj-p
title: prof. dr.
name: Primož Podržaj
role: head
photo: photo.jpg
fields:
- Krmilni sistemi
- Umetna inteligenca
- Strojni vid
career:
-
  title: LAMPA, head of the laboratory
  year: 2021
  institution: University of Ljubljana
-
  title: Professor
  year: 2021
  institution: University of Ljubljana
-
  title: Associate Professor
  year: 2015
  institution: University of Ljubljana
-
  title: LPA, head of the laboratory
  year: 2009
  institution: University of Ljubljana
-
  title: Assistant Professor
  year: 2008
  institution: University of Ljubljana
-
  title: PhD
  year: 2004
  institution: University of Ljubljana



---

