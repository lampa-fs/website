---

date: "2024-02-01"
title: LAMPA's article published in XYZ
categories:
- research
authors:
- Kozjek D.
abstract: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis malesuada non lacus at suscipit. Nulla maximus molestie lorem, sit amet auctor odio condimentum non.

---

# Lorem ipsum dolor sit amet
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis malesuada non lacus at suscipit. Nulla maximus molestie lorem, sit amet auctor odio condimentum non. Suspendisse sed metus tristique, sagittis felis congue, suscipit nunc. Phasellus at orci blandit, venenatis arcu eu, mollis lacus. Morbi consectetur risus diam, nec tempus eros sagittis sodales. Phasellus ornare hendrerit neque, at iaculis massa aliquam luctus. Vivamus pellentesque elit enim, et mattis libero vehicula accumsan. Integer feugiat vel magna quis semper. Sed placerat nibh gravida commodo ornare. Suspendisse massa urna, fermentum et tortor at, tincidunt condimentum magna. Nulla eros libero, feugiat quis ligula vitae, consectetur gravida leo. Nullam elementum ullamcorper metus eu tempus. Fusce imperdiet erat eu sem efficitur porttitor quis in enim. Cras euismod ornare metus.

## Praesent facilisis justo eu
Praesent facilisis justo eu metus pulvinar maximus. Cras vitae orci consectetur, aliquam neque vel, hendrerit enim. Proin tristique lectus pellentesque ante efficitur, nec venenatis velit sagittis. Fusce nisi erat, sodales quis suscipit vitae, tempor ac ex. In congue leo sed tempus venenatis. Vivamus sodales interdum nisl eu sodales. Pellentesque ac pretium elit, ut congue nibh. Morbi dictum diam ut eros tincidunt commodo. Nulla diam leo, commodo eu blandit in, pharetra sit amet turpis. Maecenas diam mi, bibendum consequat diam at, efficitur cursus dolor. Phasellus pretium eros non erat elementum, in ultrices tortor luctus. Nam nec quam sit amet sapien molestie rhoncus ac et lorem.

## ellentesque commodo quam id
Pellentesque commodo quam id ligula vulputate rhoncus. Cras vitae nunc quis turpis dictum lacinia sit amet nec purus. Maecenas porta condimentum leo. Morbi ligula libero, convallis nec justo vitae, rutrum porttitor risus. Vestibulum eu velit molestie, mattis nisi ut, pharetra neque. Duis eu molestie leo, a maximus erat. Curabitur vel ante efficitur, volutpat eros et, ornare dolor.
